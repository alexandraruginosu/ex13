package com.company;

public class SimpleThreadTest {

    public static void main(String[] args) {
        SimpleThread t1 = new SimpleThread("Pleasant Gardens Hotel");
        t1.start();
        SimpleThread t2 = new SimpleThread("Daydream Hotel");
        t2.start();
        SimpleThread t3 = new SimpleThread("Royal Arc Resort & Spa");
        t3.start();
    }
}
